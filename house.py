# -*- coding: utf-8 -*-  

import requests
import json
from lxml import etree
from pprint import pprint
import sys
#reload(sys)
#sys.setdefaultencoding('utf-8')
 
base_url = "https://sh.lianjia.com"
ershoufang_url = "/ershoufang"
ditiefang_url = "/ditiefang"
#global_filter = "mt1mt2l2l3a2a3ep310"
global_filter = "l2l3a2a3ep310"

def get(url, xpath=None):
    response  = requests.get(url)
    if xpath==None:
   	    return response.content.decode("utf-8")
    else:
   	    html = etree.HTML(response.content.decode("utf-8"))
   	    html_data = html.xpath(xpath)
   	    return html_data

def get_site_list():
    xpath = '//div[@data-role="ershoufang"]/div/a[starts-with(@href,"/ershoufang/")]'
    html_data = get(base_url+ershoufang_url, xpath)
    res = dict()
    for i in html_data:
   	    res[i.text.strip()] = i.get("href")
    return res

def get_ditie_list():
    xpath = '//div[@data-role="ditiefang"]/div/a[starts-with(@href,"/ditiefang/")]'
    html_data = get(base_url+ditiefang_url, xpath)
    res = dict()
    for i in html_data:
   	    res[i.text.strip()] = i.get("href")
    return res

def get_place_list_in_site(site_url):
    xpath = '//div[@data-role="ershoufang"]/div[2]/a[starts-with(@href,"/ershoufang/")]'
    html_data = get(base_url+site_url, xpath)
    res = dict()
    for i in html_data:
        res[i.text.strip()] = i.get("href")
    return res

def get_station_list_in_ditie(ditie_url):
    xpath = '//div[@data-role="ditiefang"]/div[2]/a[starts-with(@href,"/ditiefang/")]'
    html_data = get(base_url+ditie_url, xpath)
    res = dict()
    for i in html_data:
        res[i.text.strip()] = i.get("href")
    return res

def __get_house_list_from_html(html):
    xpath = '//div[@class="content "]//ul[@class="sellListContent"]/li/div[@class="info clear"]'
    html_data = html.xpath(xpath)
    res = dict()
    for house in html_data:
        title = house.xpath('div[@class="title"]/a')[0]
       	id = title.get("data-housecode")
       	house_item = dict()
       	house_item["id"] = id
       	house_item["url"] = title.get("href")
       	house_item["name"] = title.text
       	house_item["houseInfo"] = house.xpath('div[@class="address"]/div[@class="houseInfo"]/text()')[0]
       	house_item["xiaoqu"] = house.xpath('div[@class="address"]/div[@class="houseInfo"]/a/text()')[0]
       	house_item["positionInfo"] = house.xpath('div[@class="flood"]/div[@class="positionInfo"]/text()')[0]
       	house_item["followInfo"] = house.xpath('div[@class="followInfo"]/text()')[0]
       	taxfree = house.xpath('div[@class="tag"]/span[@class="taxfree"]/text()')
       	if len(taxfree)>0:
       	    house_item["taxfree"] = taxfree[0]
       	house_item["totalPrice"] = house.xpath('div[@class="priceInfo"]/div[@class="totalPrice"]/span/text()')[0]
       	house_item["unitPrice"] = house.xpath('div[@class="priceInfo"]/div[@class="unitPrice"]/@data-price')[0]
       	res[id] = house_item
	
    return res

def get_house_list(url, filter=global_filter):
    url = url+"/"+filter
    resp = get(url)
    html = etree.HTML(resp)
    res = __get_house_list_from_html(html)
    pages = html.xpath(u'//div[@class="page-box house-lst-page-box"]')
    if len(pages)>0:
        pages = pages[0]
        page_url = pages.get("page-url")
        state = json.loads(pages.get("page-data"))
        total_page = state["totalPage"]
        cur_page = state["curPage"]
        if cur_page<total_page:
            for page in range(cur_page+1, total_page+1):
                next_page_url = base_url+page_url.format(page=page)
                resp_next = get(next_page_url)
                html_next = etree.HTML(resp_next)
                res_next = __get_house_list_from_html(html_next)
                res.update(res_next)
    return res


#site_list = get_site_list()
#yangpu = get_place_list_in_site(site_list[u"虹口"])
#for place,url in yangpu.items():
#    res = get_house_list(base_url+url)
#    print(place, url, len(res))

def get_houses_by_ditie(ditie=None):
    ditie_list = get_ditie_list()
    if ditie==None:
        ditie_list = list(ditie_list.items())
        ditie_list.sort()
        for name,url in ditie_list:
            ditie = get_station_list_in_ditie(url)
            print("%s:"%name)
            for place,url in ditie.items():
                res = get_house_list(base_url+url)
                if len(res)>0:
                    print(place, url+global_filter, len(res))
            print("")
    else:
        ditie = get_station_list_in_ditie(ditie_list[ditie])
        for place,url in ditie.items():
            res = get_house_list(base_url+url)
            if len(res)>0:
                print(place, url+global_filter, len(res))
get_houses_by_ditie()

def get_houses_by_site(site=None):
    site_list = get_site_list()
    if site==None:
        site_list = site_list.items()
        site_list.sort()
        for name,url in site_list:
            site = get_place_list_in_site(url)
            print("%s:"%name)
            for place,url in site.items():
                res = get_house_list(base_url+url)
                if len(res)>0:
                    print(place, len(res), base_url+url+global_filter)
            print("")
    else:
        site = get_place_list_in_site(site_list[site])
        for place,url in site.items():
            res = get_house_list(base_url+url)
            if len(res)>0:
                print(place, len(res), base_url+url+global_filter)
#get_houses_by_site()


